<?php include('inc/head.php'); ?>
<?php include('inc/nav.php'); ?>

<main>
  <?php include('home.php'); ?>
  <?php include('configure.php'); ?>
  <?php include('inuse.php'); ?>
  <?php include('about.php'); ?>
</main>

<?php include('inc/foot.php'); ?>
