<section class="products" id="products">
  <section class="configurator">
    <figure>
      <a href=""><img src="assets/images/table.png">
      <figcaption>
        <p data-lang="EN">table lamps</p>
        <p data-lang="SL">namizne svetilke</p>
      </figcaption></a>
    </figure>
    <figure>
      <a href=""><img src="assets/images/floor.png">
      <figcaption>
        <p data-lang="EN">floor lamps</p>
        <p data-lang="SL">talne svetilke</p>
      </figcaption></a>
    </figure>
    <figure>
      <a href=""><img src="assets/images/ceiling.png">
      <figcaption>
        <p data-lang="EN">ceiling lamps</p>
        <p data-lang="SL">stropne svetilke</p>
      </figcaption></a>
    </figure>
    <figure>
      <a href=""><img src="assets/images/wall.png">
      <figcaption>
        <p data-lang="EN">wall lamps</p>
        <p data-lang="SL">stenske svetilke</p>
      </figcaption></a>
    </figure>
    <!-- below is a fix for flex -->
    <div class="image"></div>
    <div class="image"></div>
    <div class="image"></div>
  </section>
</section>