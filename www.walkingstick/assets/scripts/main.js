var logoWS = [
   ["w", "ww"],
   ["a", "aa", "aaa", "aaaa"],
   ["l", "ll"],
   ["k", "kk", "kkk"],
   ["i", "ii", "iii", "iiii", "iiiii", "iiiiii"],
   ["n", "nn", "nnn"],
   ["g", "gg", "ggg", "gggg", "ggggg"],
   [" ", " "],
   ["s", "ss", "sss"],
   ["t", "tt", "ttt"],
   ["i", "ii", "iii", "iiii", "iiiii", "iiiiii"],
   ["c", "cc", "ccc"],
   ["k", "kk", "kkk"],
];

var nodes = [
  '[++]',
  '[+++]',
  '[++++]',
  '[+++++]',
  '[++++++]',
]

var navLogo = document.querySelector('body > nav h1 a');
var logoInText = document.querySelectorAll('.logo');
var nav = document.querySelector('body > nav')
var navElements = nav.querySelectorAll('li')
var navLinks = nav.querySelectorAll('a');
var langButtons = document.querySelectorAll('.lang');
var theLangs = document.querySelectorAll('*[data-lang]');
var theLangsSL = document.querySelectorAll('*[data-lang="SL"]');
var titles = document.querySelectorAll(".title")
var listElements = document.querySelectorAll('main li');
var main = document.querySelector('main');
var sections = document.querySelectorAll('main > section');
var windowWidth = window.innerWidth;

if (windowWidth > 1000) {
  navLinks.forEach(link => {
  link.addEventListener('click', function() {
      showNavContent(link);
      randomRotateNav(link);
      linkNav(nav);
  })
})
} else {
  navElements.forEach(link => {
    link.addEventListener('click', function() {
      var href = link.querySelector('a').getAttribute('href').split('#')[1];
      document.getElementById(href).scrollIntoView();
      nav.classList.remove('visible')
    })
  })
}

randomLogo();

var totalDistance = 0;
var oldCursorX, oldCursorY;

document.addEventListener('mousemove', function(e) {
  var cursorThreshold = 150;
  if (oldCursorX) totalDistance += Math.sqrt(Math.pow(oldCursorY - e.clientY, 2) + Math.pow(oldCursorX - e.clientX, 2));
  if (totalDistance >= cursorThreshold){
    randomLogo();
    totalDistance = 0;
  }
  
  oldCursorX = e.clientX;
  oldCursorY = e.clientY;

})

if (windowWidth > 1000) {
  randomRotateNav();
}

theLangsSL.forEach(langSL => {
  langSL.style.display = 'none';
})

langButtons.forEach(button => {
  button.addEventListener('click', function() {
    langButtons.forEach(button => {
      button.classList.remove('active');
    })
    button.classList.add('active');
    var theClass = button.className.split(' ')[1];
    var theLang = document.querySelectorAll('*[data-lang="' + theClass + '"');
    theLangs.forEach(langs =>  {
      langs.style.display = 'none';
    })
    theLang.forEach(lang => {
      lang.style.display = '';
    })
  if (windowWidth > 1000) {
      linkNav(nav);
  }
})
})

if (windowWidth <= 1000) {
  var burger = document.querySelector('.burger');
  burger.addEventListener('click', function() {
    nav.classList.toggle('visible');
  })
}

window.addEventListener('load', function () {
  if (windowWidth > 1000) {
    linkNav(nav);
  }
}, false);

window.addEventListener('resize', function() {
  if (windowWidth > 1000) {
    linkNav(nav);
  }
})

if (windowWidth > 1000){
window.onscroll = () => {
  var current = "";

  sections.forEach((section) => {
    const sectionTop = section.offsetTop;
    if (scrollY + window.innerHeight / 4 >= sectionTop ) {
      section.classList.add('current');
      current = section.getAttribute("id"); 
    }
  });

  navElements.forEach((li) => {
    if (li.classList.contains(current)) {
      if (!li.classList.contains('active')) {
        var links = li.querySelectorAll('a.title');
        li.classList.add("active");
        links.forEach(link => {
          showNavContent(link);
          randomRotateNav(link);
          linkNav(nav);
        })
      }
    } else {
      li.classList.remove("active");
    }
  });
};
}

function randomLogo() {
  var logo = '';
  logoWS.forEach(letter => {
    var nbLetter = letter.length;
    var randomLetter = Math.floor(Math.random() * (nbLetter - (0)) + (0))
    logo += letter[randomLetter];
  })
  
  navLogo.innerHTML = logo;
  logoInText.forEach(log => {
    log.innerHTML = logo;
  });
}

function randomRotateNav(link) {
  var navElements = nav.querySelectorAll('li');
  var i = 1;
  var max;
  var current = link ? link.parentNode : '';
  navElements.forEach(el => {
    if (el == navElements[0] || i == navElements.length || el == current) {
      min = 0;
      max = 0;
    } else if (i != navElements.length) {
      min = 20;
      max = 70;
    }
    var randomRotate = Math.floor(Math.random() * (max - (min)) + (min))
    el.style.transform = 'rotate(' + randomRotate + 'deg)';
    el.style.transition = 'transform .05s ease-in';
    i++
  });
}

function linkNav(nav) {
  
  var svgs = document.querySelectorAll('svg.line');
  if (svgs) {
    svgs.forEach(svg => {
      svg.remove();        
    })
  }

  setTimeout(() => {
    var navElementsToJoin = nav.querySelectorAll('.join');
    var i = 0;
    navElementsToJoin.forEach(el => {
      if (navElementsToJoin[i + 1]) {
        var existingLine = document.querySelectorAll('svg.line-' + i);
        if (existingLine) {
          existingLine.forEach(svg => {
            svg.remove();        
          })
        }
        var pointRight = el.querySelector('.point-right');
        var pointLeft = navElementsToJoin[i + 1].querySelector('.point-left');
        var pointRightTop = Math.ceil(pointRight.getBoundingClientRect().top);
        var pointRightleft = Math.ceil(pointRight.getBoundingClientRect().left);
        var pointLeftTop = Math.ceil(pointLeft.getBoundingClientRect().top);
        var pointLeftleft = Math.ceil(pointLeft.getBoundingClientRect().left);
        var x1 = pointRightleft + pointRight.offsetWidth / 2;
        var y1 = pointRightTop + pointRight.offsetHeight / 2;
        var x2 = pointLeftleft + pointLeft.offsetWidth / 2;
        var y2 = pointLeftTop + pointLeft.offsetHeight / 2;
        var svgElement =  document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svgElement.classList.add('line');
        svgElement.classList.add('line-' + i);
        svgElement.setAttribute('width', 1500);
        svgElement.setAttribute('height', 1500);
        svgElement.innerHTML = '<line x1="' + x1 + '" y1="' + y1 + '" x2="' + x2 + '" y2="' + y2 + '" style="stroke:rgb(0,0,0);stroke-width:1" />';
        nav.appendChild(svgElement);
      }
      i++;
    })
  }, 400);
}

function showNavContent(link) {

  var content = link.parentNode.querySelector('.content');
  var node = link.parentNode.querySelector('.point-right');
  var otherContent = nav.querySelector('.content.visible');
  var otherNode = nav.querySelector('.point-right.absolute');
  if (otherNode) {
    otherNode.classList.remove('absolute');
  }
  if (otherContent) {
    otherContent.classList.remove('visible');
  }
  if (content) {
    content.classList.add('visible')
    node.classList.add('absolute');
  }
}
