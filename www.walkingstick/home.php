<section class="home" id="home">
  <img src="assets/images/anim.gif">
  <p data-lang="EN"><span class="logo">walking stick</span> are a group of highly camouflaged insects. They confuse predators by blending into plant material. They may even sway back and forth to resemble a twig or a leaf moving in the wind.</p> 
  <p data-lang="SL">Paličnjaki sodijo med posnemalce, red žuželk z izjemno visoko stopnjo prilagajanja okolju. Paličnjaki s posnemanjem rastlinskih oblik zmedejo plenilce. Nekateri se celo zibajo sem ter tja, kot suhe vejice in listi v vetru.</p> 
</section>
