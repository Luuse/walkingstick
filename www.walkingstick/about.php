<section id="about" class="about">

    <section class="contact">
      <p data-lang="EN">The <span class="logo">walking stick></span> come to life in our workshop in Lubljana, slovenia. Since we are pretty obsessed with the highest quality standards, we work only with the best Slovene wood connoisseur and funiture manufacturer</p>
      <p data-lang="EN">We want our furniture to be reconfigurable and adoptable, to follow your most creative inner impulses, to adopt your best camouflage -  and to survive through generations!</p>   
      <p data-lang="SL">Sprehajalna palica je zaživela v naši delavnici v Ljubljani, Slovenija. Ker smo precej obsedeni z najvišjimi standardi kakovosti, sodelujemo samo z najboljšimi slovenskimi poznavalci lesa in proizvajalci pohištva.</p>
      <p data-lang="SL">Želimo, da je naše pohištvo nastavljivo in prilagodljivo, da sledi vašim najbolj kreativnim notranjim impulzom, da sprejme vašo najboljšo kamuflažo - in da preživi skozi generacije!</p>
    </section>

    <section class="flex">
      <div class="info">
        <h2 data-lang="EN">Contact</h2>
        <h2 data-lang="SL">Kontakt</h2>
        <ul>
          <li><span class="bouiboui"></span> <span class="logo">walking stick</span></li>
          <li><span class="bouiboui"></span> Happyness street 49</li>
          <li><span class="bouiboui"></span> Ljubljana, Slovenia</li>
          <li><span class="bouiboui"></span><a href="mailto:contact@walkingstick.sl">contact@walkingstick.sl</a></li>
        </ul>
      </div>
      <div class="info">
        <h2 data-lang="EN">Credits</h2>
        <h2 data-lang="SL">krediti</h2>
        <ul>
          <li data-lang="EN">graphic design: <a href="http://luuse.io">Luuse</a></li>
          <li data-lang="SL">grafično oblikovanje: <a href="http://luuse.io">Luuse</a></li>
          <li data-lang="EN">development: <a href="https://www.markodamis.com/">Marko Damiš</a> and <a href="http://luuse.io">Luuse</a></li>
          <li data-lang="SL">razvoj: <a href="https://www.markodamis.com/">Marko Damiš</a> in <a href="http://luuse.io">Luuse</a></li>
        </ul>
      </div>
    </section>
  
</section>