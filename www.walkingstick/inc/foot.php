  <footer>
    <div><span class="lang EN active">EN</span> / <span class="lang SL">SL</span></div>
    <div class="mobile"><span class="logo">walking stick</span></div>
    <div class="mobile burger">☰</div>
  </footer>
  <script src="assets/scripts/main.js"></script>
</body>
</html>