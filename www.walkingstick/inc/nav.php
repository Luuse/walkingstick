<nav>
  <ul>
    <li class="join home active">
      <h1><span class="point-right">[++++++]</span><a href="#home" class="current home title">walking stick</a></h1>
    </li>
    <li class="join products"><span class="point-left">[++++++]</span>
      <a href="#products" class="title" data-lang="EN">let's configure</a>
      <a href="#products" class="title" data-lang="SL">konfigurirajmo</a>
      <div class="content">
        <p data-lang="EN">We encourage you to find your own <span class="logo">walking stick</span>. It will take only four easy steps to get it done!</p>
        <p data-lang="SL">Spodbujamo vas, da poiščete svojo sprehajalno palico. Potrebovali boste samo štiri preproste korake, da to storite!</p>
      </div>
      <span class="point-right">[++++++]</span>
    </li>
    <li class="join inuse">
      <span class="point-left">[++++++]</span>
      <a class="title" href="#inuse" data-lang="EN">your <span class="logo">walking stick</span></a>
      <a class="title" href="#inuse" data-lang="SL">svojo sprehajalno palico</a>
      <div class="content">
        <p data-lang="EN">You will be surprised how smartly your <span class="logo">walking stick</span> blend into your habitat. If you want others to admire your creation, please email your images to <a href="mailto:home@walkingstick.com">home@walkingstick.com</a>.</p>
        <p data-lang="SL">Presenečeni boste, kako pametno se vaša sprehajalna palica zlije z vašim življenjskim okoljem. Če želite, da drugi občudujejo vašo stvaritev, pošljite svoje slike po e-pošti na <a href="mailto:home@walkingstick.com">home@walkingstick.com</a>.</p>
      </div>
      <span class="point-right">[++++++]</span>
    </li>
    
    <li class="join about"><span class="point-left">[++++++]</span>
      <a href="#about" class="title" data-lang="EN">we are</a>
      <a href="#about" class="title" data-lang="SL">mi smo</a>
    </li>
  </ul>
</nav>