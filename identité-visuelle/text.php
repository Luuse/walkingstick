<!DOCTYPE html>
<html>

<head>
    <title></title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style>

    	@font-face {
  font-family: 'baskervol';
  src:  url('fonts/BBBBaskervvol-Base.woff2') format('woff2'),
        url('fonts/BBBBaskervvol-Base.woff') format('woff');
}

@font-face {
  font-family: 'garamond';
  src:  url('fonts/EBGaramond08-Regular.woff2') format('woff2'),
        url('fonts/EBGaramond12-Regular.woff') format('woff');
}

@font-face {
  font-family: 'adelphe';
  src:  url('fonts/Adelphe-FlorealRegular.woff') format('woff2'),
        url('fonts/Adelphe-FlorealRegular.woff') format('woff');
}

@font-face {
  font-family: 'paragon';
  src:  url('fonts/paragon/Paragon-Regular.otf');
}



    	body{
    		padding: 0 1.5rem;
    	}
    	img{
    		width: 3%;

    	}

    	img svg{
    		width: 100%;
    	}
    	.line{
    		display: block;
    	}

    	#gif > div{
    		display: inline;

    	}
    	#gif > div > img{
    		display: none;
    		width: 4%;
    	}

    	#gif > div > img:first-of-type{
    		display: inline-block;

    	}

			#text {
				max-width: 1100px;
				margin: 1rem 0;
				margin-left: 4rem;
			}

    	#text img{
    		width: 1.5%;
    	}
    	.space{
    		display: inline-block;
    		width: 1.5%;
    	}

    	main > div{
    		/* margin-bottom: 4rem; */
    	}
    	h1{
    		margin-bottom: 0rem;
    	}
    	p{
    		margin: 0;
				max-width: 1100px;
				font-family:'paragon';
				font-size: 20px; 
				line-height: 1.4;
				text-indent: 4rem;
    	}

			p:nth-of-type(1),
			#text + p {
				text-indent: 0;
			}

    	#font{
    		display: flex;
    		column-gap:2rem;
    		flex-wrap: wrap;
    	}
    	#font > div{
    		width: 45%;
    	}
    	#font img{
    		width: 2rem;
    	}

			.title {
				margin-bottom: 0;
			}
    </style>

    <!-- <script src="js/jquerymin.js"></script> -->
</head>

<body>
	  <main>

		<div>
			<h1>
				<span class="random"><img src='svg/reg/p_asterisk1.svg'></span>
				<span class="random"><img src='svg/reg/b_w.svg'></span>
				<span class="random"><img src='svg/reg/b_a3.svg'></span>
				<span class="random"><img src='svg/reg/b_l.svg'></span>
				<span class="random"><img src='svg/reg/b_k.svg'></span>
				<span class="random"><img src='svg/reg/b_i.svg'></span>
				<span class="random"><img src='svg/reg/b_n1.svg'></span>
				<span class="random"><img src='svg/reg/b_g1.svg'></span>
				<span class="space"></span>
				<span class="random"><img src='svg/reg/b_s.svg'></span>
				<span class="random"><img src='svg/reg/b_t.svg'></span>
				<span class="random"><img src='svg/reg/b_i.svg'></span>
				<span class="random"><img src='svg/reg/b_c.svg'></span>
				<span><img src='svg/reg/b_k.svg'></span>
				<span class="random"><img src='svg/reg/p_asterisk1.svg'></span>
			</h1>
			<p>
				Walking sticks are one of many species that can reproduce parthenogenetically, meaning the females can produce unfertilized eggs that hatch and grow into new females. Females lay eggs that look like seeds, and they have numerous egg-laying mechanisms to keep predators away. Some females lay eggs in places that are hidden or hard to get to. Others drop eggs one by one on the ground so they’re not all in one place for a predator to find. Newly hatched walking sticks reach adult size once they’ve undergone several molts. They reach maturity between three months and one year, and usually live up to two years.  Lorem ipsum dolor sit amet consectetur, adipisicing elit. Voluptate ab distinctio eos ducimus accusamus sint aperiam perspiciatis totam, illum itaque dolore unde nesciunt aliquam, ut dolorem officia, error quaerat corporis!
			</p>
		</div>

	<div id="text">

<span class="random"><img src='svg/bold/b_w.svg'></span>

		<!-- Pardon pour cette div je suis une bourrine -->

<span class="random"><img src='svg/bold/b_a.svg'></span>
<span class="random"><img src='svg/bold/b_l.svg'></span>
<span class="random"><img src='svg/bold/b_k.svg'></span>
<span class="random"><img src='svg/bold/b_i.svg'></span>
<span class="random"><img src='svg/bold/b_n.svg'></span>
<span class="random"><img src='svg/bold/b_g.svg'></span>
<span class="space"></span>
<span class="random"><img src='svg/bold/b_s.svg'></span>
<span class="random"><img src='svg/bold/b_t.svg'></span>
<span class="random"><img src='svg/bold/b_i.svg'></span>
<span class="random"><img src='svg/bold/b_c.svg'></span>
<span><img src='svg/bold/b_k.svg'></span>
<span><img src='svg/bold/b_s.svg'></span>
<span><img src='svg/bold/p_asterisk.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_p.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_f.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_f.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_p.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_p.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_p.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/p_asterisk.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_j.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_v.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_w.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_f.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_w.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_v.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_w.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span><img src='svg/reg/p_asterisk.svg'></span>

	</div>
	<div>	

 </div>

	<p>
			Walking sticks are one of many species that can reproduce parthenogenetically, meaning the females can produce unfertilized eggs that hatch and grow into new females. Females lay eggs that look like seeds, and they have numerous egg-laying mechanisms to keep predators away. Some females lay eggs in places that are hidden or hard to get to. Others drop eggs one by one on the ground so they’re not all in one place for a predator to find. Newly hatched walking sticks reach adult size once they’ve undergone several molts. They reach maturity between three months and one year, and usually live up to two years. 
		</p>
	<div id="text" class="title">
		<img src='svg/bold/p_asterisk2.svg'>
		<img src='svg/bold/p_asterisk1.svg'>
		<img src='svg/bold/p_asterisk2.svg'>
		<img src='svg/bold/p_asterisk1.svg'>
		<img src='svg/bold/p_asterisk2.svg'>
		<span class="space"></span>
		<span><img src='svg/bold/b_l.svg'></span>
		<span><img src='svg/bold/b_i.svg'></span>
		<span><img src='svg/bold/b_f.svg'></span>
		<span><img src='svg/bold/b_e.svg'></span>
		<span class="space"></span>
		<span><img src='svg/bold/b_h.svg'></span>
		<span><img src='svg/bold/b_i.svg'></span>
		<span><img src='svg/bold/b_s.svg'></span>
		<span><img src='svg/bold/b_t.svg'></span>
		<span><img src='svg/bold/b_o.svg'></span>
		<span><img src='svg/bold/b_r.svg'></span>
		<span><img src='svg/bold/b_y.svg'></span>
		<span class="space"></span>
		<img src='svg/bold/p_asterisk2.svg'>
		<img src='svg/bold/p_asterisk1.svg'>
		<img src='svg/bold/p_asterisk2.svg'>
		<img src='svg/bold/p_asterisk1.svg'>
		<img src='svg/bold/p_asterisk2.svg'>
	</div>
	<p>
		Walking sticks are one of many species that can reproduce parthenogenetically, meaning the females can produce unfertilized eggs that hatch and grow into new females. Females lay eggs that look like seeds, and they have numerous egg-laying mechanisms to keep predators away. Some females lay eggs in places that are hidden or hard to get to. Others drop eggs one by one on the ground so they’re not all in one place for a predator to find. Newly hatched walking sticks reach adult size once they’ve undergone several molts. They reach maturity between three months and one year, and usually live up to two years. 
	</p>
	<p>Slovenski jezik prvotno pomeni slovanski jezik. Nemci so slovenščino nekoč imenovali »wendisch« oz. »Windisch«, Slovenci pa tudi kranjski jezik, vendar se to pokrajinsko ime zanj ni uveljavilo. Slovenski prebivalci Koroške in Štajerske so se imenovali Slovenci že v 18. stol. Koren sloven- je bil pri Slovanih znan tudi v Rusiji ob Labi na sedanjem Nemškem (Slovinci), na Slovaškem, pa tudi na hrvaškem kajkavskem (Zagreb, Čakovec do pred Karlovcem) in še celo čakavskem področju ob Jadranskem morju na jugu vse do Dubrovnika (slovinski). Hrvati naš jezik imenujejo slovenski, Srbi slovenački, Rusi slovenskij, Nemci Slowenisch, angleško njegovo ime je Slovene/Slovenian. Latinski izraz zanj je lingua Slavonica oz. lingua Slovenica. Tudi Slovaki imenujejo svoj jezik slovenský, prebivalke so Slovenke, jezik tudi slovenčina, deželo svojo pa Slovensko.</p>
	<p>    a, b, c, č, ć, d, đ, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, š, t, u, v, w, x, y, z, ž.</p>

 <!-- <div id='font'>
<div>
<h1>
	<span class="random"><img src='svg/bold/b_w.svg'></span>
	<span class="random"><img src='svg/bold/b_a3.svg'></span>
	<span class="random"><img src='svg/bold/b_l.svg'></span>
	<span class="random"><img src='svg/bold/b_k.svg'></span>
	<span class="random"><img src='svg/bold/b_i.svg'></span>
	<span class="random"><img src='svg/bold/b_n1.svg'></span>
	<span class="random"><img src='svg/bold/b_g1.svg'></span>
	<span class="space"></span>
	<span class="random"><img src='svg/bold/b_s.svg'></span>
	<span class="random"><img src='svg/bold/b_t.svg'></span>
	<span class="random"><img src='svg/bold/b_i.svg'></span>
	<span class="random"><img src='svg/bold/b_c.svg'></span>
	<span><img src='svg/bold/b_k.svg'></span>
</h1>
<p style="font-family:'baskervol';font-size: 20px;">
	Walking sticks are one of many species that can reproduce parthenogenetically, meaning the females can produce unfertilized eggs that hatch and grow into new females. Females lay eggs that look like seeds, and they have numerous egg-laying mechanisms to keep predators away. Some females lay eggs in places that are hidden or hard to get to. Others drop eggs one by one on the ground so they’re not all in one place for a predator to find. Newly hatched walking sticks reach adult size once they’ve undergone several molts. They reach maturity between three months and one year, and usually live up to two years. 
</p>
</div>



<div>
<h1>
	<span class="random"><img src='svg/bold/b_w.svg'></span>
	<span class="random"><img src='svg/bold/b_a2.svg'></span>
	<span class="random"><img src='svg/bold/b_l1.svg'></span>
	<span class="random"><img src='svg/bold/b_k.svg'></span>
	<span class="random"><img src='svg/bold/b_i2.svg'></span>
	<span class="random"><img src='svg/bold/b_n.svg'></span>
	<span class="random"><img src='svg/bold/b_g2.svg'></span>
	<span class="space"></span>
	<span class="random"><img src='svg/bold/b_s.svg'></span>
	<span class="random"><img src='svg/bold/b_t.svg'></span>
	<span class="random"><img src='svg/bold/b_i.svg'></span>
	<span class="random"><img src='svg/bold/b_c.svg'></span>
	<span><img src='svg/bold/b_k.svg'></span>
</h1>
<p style="font-family:'garamond';font-size: 20px;line-height: 1.5;">
	Walking sticks are one of many species that can reproduce parthenogenetically, meaning the females can produce unfertilized eggs that hatch and grow into new females. Females lay eggs that look like seeds, and they have numerous egg-laying mechanisms to keep predators away. Some females lay eggs in places that are hidden or hard to get to. Others drop eggs one by one on the ground so they’re not all in one place for a predator to find. Newly hatched walking sticks reach adult size once they’ve undergone several molts. They reach maturity between three months and one year, and usually live up to two years. 
</p>
</div>
<div>
<div> -->
   </main>
  </body>
  <script type="text/javascript">

		

var random = document.querySelectorAll('#text span img');

console.log(random);

Array.from(random).forEach((element, index) => {
	var lettre = element.getAttribute('src');
	console.log(lettre);
	if (lettre.includes('b_a')){
		var arrayLettre = ['b_a', 'b_a1', 'b_a2', 'b_a3'];
		var randomLettre = arrayLettre[Math.floor(Math.random() * arrayLettre.length)];
		var newLettre = lettre.replace('b_a', randomLettre);
	element.setAttribute('src', newLettre);

	}else if (lettre.includes('b_l')){
		
	element.setAttribute('src', replaceRandom('b_l', ['b_l', 'b_l1'], lettre));
	}else if(lettre.includes('b_i')){
		element.setAttribute('src', replaceRandom('b_i', ['b_i','b_i2'], lettre));
	}else  if (lettre.includes('b_n')){
		element.setAttribute('src', replaceRandom('b_n', ['b_n', 'b_n1'], lettre));
	}else if (lettre.includes('b_g')){
		element.setAttribute('src', replaceRandom('b_g', ['b_g', 'b_g1', 'b_g2'], lettre));
	}else if (lettre.includes('p_asterisk')){
		element.setAttribute('src', replaceRandom('p_asterisk', ['p_asterisk1', 'p_asterisk'], lettre));
	}

	

  // conditional logic here.. access element
});



  function replaceRandom(maLettre, arrayLettre, maSource){
  	var randomLettre = arrayLettre[Math.floor(Math.random() * arrayLettre.length)];
  	var newLettre = maSource.replace(maLettre, randomLettre);
  	return newLettre;
  }

 


	</script>
	</html>
