# Walking Stick Charte graphique

## Typographies

### Walking-Ocr-reg
L'identité visuelle de Walking Stick se compose d'une multitude de logos légèrement différenciés par les caractères alternatifs. 

La typographie du lettrage du logo est une version de l'OCR-Pbi, dessinée par Luuse et dont les tracés sont herités de l'OCR-B. Elle est sous licence libre (SIL-OFL).

Cette fonte, dont certaines lettres (a-c-g-i-k-l-n-s-t-w) possèdent 2 à 4 tracés alternatifs, construit à la fois l'identité visuelle du projet, et permet également, pour des titres et autres textes à mettre en avant, de représenter la notion phare du projet: la modularité. L'apparition des caractères alternatifs ont été developpés de sorte à intervenir de manière aléatoire et non controlable par l'utilisateur.

<span class="WS">aaaacccggggiiiikkllnnnssstttww</span>

<span class="WS">wwaaallkkiiiinnnggg ssstttiiiccckk</span>

<span class="WS">walking stick</span>

En plus de cet alphabet variable, l'implantation d'autres caractères typographiques herités des fixations du projet Walking Stick viendront intervenir dans le traitement de texte afin de ponctuer/ornementer, le propos. Son usage est libre.
Pour faire apparaître ces différents nœuds de fixation, la syntaxe est la suivante:

\[\*\*\] -> <span class="WS">\[\*\*\]</span> 

\[\*\*\*\] -> <span class="WS">\[\*\*\*\]</span> 

\[\*\*\*\*\] -> <span class="WS">\[\*\*\*\*\]</span> 

\[\*\*\*\*\*\] -> <span class="WS">\[\*\*\*\*\*\]</span> 

\[\*\*\*\*\*\*\] -> <span class="WS">\[\*\*\*\*\*\*\]</span> 


La même syntaxe pourra être utilisée pour des nœuds plus grands, avec l'emploi du signe +



\[++\]		-> <span class="WS">\[++\]</span>

\[+++\]		-> <span class="WS">\[+++\]</span>

\[++++\]	-> <span class="WS">\[++++\]</span>

\[+++++\]	-> <span class="WS">\[+++++\]</span>

\[++++++\]	-> <span class="WS">\[++++++\]</span>


### Paragon
Paragon est une typographie dessinée en 2019. C'est la fonte qui sera utilisée pour tout les textes courant. 

Les deux typographies se trouvent dans le dossier Fonts.

##Logo
Un dossier contenant différents logos est à disposition des utilisateurs, leur donnant la possibilité de chosir celui qui s'adaptera le mieux au support/propos.
les types de fichiers ne sont pas les mêmes selon le support sur lequel le logo va intervenir. Le SVG, format contenant uniquement des tracés vectoriels peut être utilisé pour tout type de supports (numériques ou materiels), il est le format recommandé.

## Type de formats de fichier
Pour du print: 
 
 - .svg
 - .tiff

Pour l'écran:

 - .svg
 - .png (transparence)
 - .gif (animation)

## usage du logo
La finesse du tracé impliquera un usage du logo préférablement sur fond uni.
La superposition du logo sur une image complexe (photographie contenant des éléments contrastés et petits par exemple) n'est pas recommandé.

## Déclainaison possible
Selon les futurs action que va mener le projet walking stick, le logo pourra s'appliquer sur différents supports s'adaptant sans trop d'effort aux futurs objets utilisés par l'équipe. (ruban adhesif pour empaquetage des meubles, tampons, marquage au fer chaud sur le mobilier, découpe laser, gravure sur plaque etc.)

## La palette de couleur
Les couleurs ne sont pas définitives ici, mais donnent un point de départ pour la colorisation du logo si besoin. Il est préférable d'utiliser la même couleur pour les lettres, les ornements cependant peuvent varier. Là encore tout dépendra de son utilisation.















