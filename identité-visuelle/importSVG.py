# /usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import glob
import sys
import lxml.etree as ET

NameFile = sys.argv[1]

compositeChar = [192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 210, 211, 212, 213, 214, 217, 218, 219, 220, 224, 225, 226, 227, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 242, 243, 244, 249, 250, 251, 252, 268, 269, 282, 283, 327, 328, 344, 345, 352, 353, 338, 381, 382, 339, 366, 367]

font = fontforge.open('temp/font.sfd')
svg_final = glob.glob('svg/reg/*svg')

line_code = "charstart("
line_chasse = 'width="'

def removeCadra(root, pattern):
    for child in root:
        if child.tag == '{http://www.w3.org/2000/svg}path':
            if child.attrib['style'].startswith(pattern):
                b = child
                root.remove(b)
    ET.dump(root)
    return ET.tostring(root, encoding='utf8', method='xml').decode()

for letter_svg in svg_final:
    letter = letter_svg.split("/")[-1].replace(".svg", "")
    print(letter)

    mp_file = open("letters/"+letter+".mp", "r")
    for ligne in mp_file:
        if line_code in ligne:
            val_code = ligne.split("(")[-1].split(")")[-2]
    mp_file.close()

    svg_path = "svg/reg/"+letter+".svg"

    with open(svg_path, 'rb') as gp:
        treeLet = ET.parse(gp)
    rootLet = treeLet.getroot()
    gclean = removeCadra(rootLet, 'stroke:rgb(100.000000%,0.000000%,0.000000%);')
    out_svg = svg_path 
    f = open(svg_path, 'w')
    f.write(gclean) 
    f.close()

    svg_file = open(svg_path, "r")
    for ligne in svg_file:
        if line_chasse in ligne:
            val_chasse = ligne.split('width="')[-1].split('" height="')[-2]
            val_chasse = (float(val_chasse) / 2) / 1.075
            # print('la valeur des de', val_chasse)
    svg_file.close()
    if '.ss0' in letter:
        glyph = font.createChar(-1, letter[2:])
    else:
        glyph = font.createChar(int(val_code))
    # if letter == 'b_a':
        # glyph.addPosSub('aalt', 'a.ss01')
    # glyph.width = val_chasse
    print(val_code)
    glyph.width = 611
    glyph.importOutlines(letter_svg)
    glyph.correctDirection()

for letter_comp in compositeChar:
    print(letter_comp)
    glyphAcc = font.createChar(letter_comp)
    glyphAcc.width = 611
    glyphAcc.build()
font.fontname = 'Ocr-Pbi-'+NameFile
font.familyname = 'Ocr-Pbi'
font.generate('FINAL/Ocr-Pbi-'+NameFile+'.sfd')
font.generate('FINAL/Ocr-Pbi-'+NameFile+'.ttf')
font.generate('FINAL/Ocr-Pbi-'+NameFile+'.otf')
