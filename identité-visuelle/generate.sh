LETTRE=$1
GRAISSE=$2


if [ $LETTRE = "all" ];
then 
	search_dir=letters
	for entry in "$search_dir"/*
	do
  	echo "$entry"
  	myVar=`basename "$entry"`
  	hbday="${myVar//.mp/}"
  	echo $hbday

  	mpost -interaction=batchmode -s 'outputformat="svg"' "$entry"
  	mv $hbday*.svg svg/$GRAISSE/$hbday.svg
  	rm *.log *.svg
	done
else
	mpost -interaction=batchmode -s 'outputformat="svg"' letters/$LETTRE.mp
	mv $LETTRE*.svg svg/$GRAISSE/$LETTRE.svg
	rm *.log *.svg
fi