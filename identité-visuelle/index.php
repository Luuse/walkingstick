<!DOCTYPE html>
<html>

<head>
    <title></title>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <style>

    	@font-face {
  font-family: 'baskervol';
  src:  url('fonts/BBBBaskervvol-Base.woff2') format('woff2'),
        url('fonts/BBBBaskervvol-Base.woff') format('woff');
}

@font-face {
  font-family: 'garamond';
  src:  url('fonts/EBGaramond08-Regular.woff2') format('woff2'),
        url('fonts/EBGaramond12-Regular.woff') format('woff');
}

@font-face {
  font-family: 'adelphe';
  src:  url('fonts/Adelphe-FlorealRegular.woff') format('woff2'),
        url('fonts/Adelphe-FlorealRegular.woff') format('woff');
}



    	body{
    		padding: 2rem;
    	}
    	img{
    		width: 4%;

    	}

    	img svg{
    		width: 100%;
    	}
    	.line{
    		display: block;
    	}

    	#gif > div{
    		display: inline;

    	}
    	#gif > div > img{
    		display: none;
    		width: 4%;
    	}

    	#gif > div > img:first-of-type{
    		display: inline-block;

    	}

    	#text img{
    		width: 4%;
    	}
    	#text .space{
    		display: inline-block;
    		width: 4%;
    	}

    	main > div{
    		margin-bottom: 4rem;
    	}
    	h1{
    		margin-bottom: 0rem;
    	}
    	p{
    		margin: 0;
    	}

    	#font{
    		display: flex;
    		column-gap:2rem;
    		flex-wrap: wrap;
    	}
    	#font > div{
    		width: 45%;
    	}
    	#font img{
    		width: 2rem;
    	}
    </style>

    <!-- <script src="js/jquerymin.js"></script> -->
</head>

<body>
	  <main>
	<div id="text">

		<span class="random"><img src='svg/bold/b_w.svg'></span>

		<!-- Pardon pour cette div je suis une bourrine -->

<span class="random"><img src='svg/bold/b_a.svg'></span>
<span class="random"><img src='svg/bold/b_l.svg'></span>
<span class="random"><img src='svg/bold/b_k.svg'></span>
<span class="random"><img src='svg/bold/b_i.svg'></span>
<span class="random"><img src='svg/bold/b_n.svg'></span>
<span class="random"><img src='svg/bold/b_g.svg'></span>
<span class="space"></span>
<span class="random"><img src='svg/bold/b_s.svg'></span>
<span class="random"><img src='svg/bold/b_t.svg'></span>
<span class="random"><img src='svg/bold/b_i.svg'></span>
<span class="random"><img src='svg/bold/b_c.svg'></span>
<span><img src='svg/bold/b_k.svg'></span>
<span><img src='svg/bold/b_s.svg'></span>
<span class="space"></span>
<span><img src='svg/bold/p_asterisk1.svg'></span>
<span><img src='svg/bold/p_asterisk2.svg'></span>
<span><img src='svg/bold/p_asterisk3.svg'></span>


<span class="space"></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk1.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_p.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_f.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_f.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk1.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_p.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_p.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_p.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/p_asterisk1.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk1.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_j.svg'></span>
<span><img src='svg/reg/b_u.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/p_asterisk1.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_v.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_w.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_k.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_f.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_c.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_y.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_r.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_s.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_b.svg'></span>
<span><img src='svg/reg/b_l.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_a.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_w.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_m.svg'></span>
<span><img src='svg/reg/b_o.svg'></span>
<span><img src='svg/reg/b_v.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_g.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_t.svg'></span>
<span><img src='svg/reg/b_h.svg'></span>
<span><img src='svg/reg/b_e.svg'></span>
<span class="space"></span>
<span><img src='svg/reg/b_w.svg'></span>
<span><img src='svg/reg/b_i.svg'></span>
<span><img src='svg/reg/b_n.svg'></span>
<span><img src='svg/reg/b_d.svg'></span>
<span><img src='svg/reg/p_asterisk1.svg'></span>

	</div>
	<div>
<?php

 function get_all_directory_and_files($dir){

 	$array = array();
 
     $dh = new DirectoryIterator($dir);   
     // Dirctary object 
     foreach ($dh as $item) {
         if (!$item->isDot()) {
            if ($item->isDir()) {
                get_all_directory_and_files("$dir/$item");
            } else {
            	if ($item->getFilename() != '.DS_Store'){
                array_push($array, $dir . "/" . $item->getFilename());
            }
            }
         }



      }
      sort($array);
		foreach ($array as $key => $svg) {
		    echo "<img src = '".$svg ."'>";
		    
		}

		// foreach ($array as $key => $svg) {
		    
		//     echo "<img src = 'svg/bold/". $svg ."'>";
		// }

      
   }


 
  # Call function 
  
  get_all_directory_and_files("svg");


?> 

	

 </div>

 
   </main>
  </body>
  <script type="text/javascript">

		

var random = document.querySelectorAll('#text span img');

console.log(random);

Array.from(random).forEach((element, index) => {
	var lettre = element.getAttribute('src');
	console.log(lettre);
	if (lettre.includes('b_a')){
		var arrayLettre = ['b_a', 'b_a1', 'b_a2', 'b_a3'];
		var randomLettre = arrayLettre[Math.floor(Math.random() * arrayLettre.length)];
		var newLettre = lettre.replace('b_a', randomLettre);
	element.setAttribute('src', newLettre);

	}else if (lettre.includes('b_l')){
		
	element.setAttribute('src', replaceRandom('b_l', ['b_l', 'b_l1'], lettre));
	}else if(lettre.includes('b_i')){
		element.setAttribute('src', replaceRandom('b_i', ['b_i','b_i2'], lettre));
	}else  if (lettre.includes('b_n')){
		element.setAttribute('src', replaceRandom('b_n', ['b_n', 'b_n1'], lettre));
	}else if (lettre.includes('b_g')){
		element.setAttribute('src', replaceRandom('b_g', ['b_g', 'b_g1', 'b_g2'], lettre));
	}else if (lettre.includes('p_asterisk')){
		element.setAttribute('src', replaceRandom('p_asterisk1', ['p_asterisk1', 'p_asterisk2','p_asterisk3','p_asterisk4'], lettre));
	}

	

  // conditional logic here.. access element
});



  function replaceRandom(maLettre, arrayLettre, maSource){
  	var randomLettre = arrayLettre[Math.floor(Math.random() * arrayLettre.length)];
  	var newLettre = maSource.replace(maLettre, randomLettre);
  	return newLettre;
  }

 


	</script>
	</html>
