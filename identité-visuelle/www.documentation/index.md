# Walking Stick - visual identity


From the file and the furniture sent, it seems clear to us that it is a question of shaping a visual identity close to the notions of __modularity__, assemblies and __construction__ with a focus on the __line__. 

With the idea of modularity in typographic design in mind, we immediately thought of Paul Renner's __Futura__, designed in 1927, based on the assembly of simple geometric modules (circle, triangle, square), in the spirit of the Bauhaus. 

![](img/futura.jpg)
![](img/futura2.jpg)

The minimalism, the reduction of shapes and the presence of __alternative glyphs__ were a starting point. We then decided to push the drawing by the line, finer, in order to distance itself from Futura, while keeping some of its anatomical concepts. 


After several graphic intuitions experimented upstream, we propose to you to discover a __first version__ affirmed in this direction.

It is a lettering using the __OCR-Pbi__ font, which Luuse had the opportunity to develop before. This typeface is a continuation of the OCR B designed by the Swiss designer Adrian Frutiger in 1968. Luuse designed this typeface with the Metapost program, which specializes in producing diagrams in PostScript language from a geometric and algebraic description. The language allows, using the syntax of the Metafont language, to combine lines, curves, points and perform geometric transformations. 
![](img/ocr.png)
![](img/meta.png)
![](img/meta2.png)

Thus, the letters that make up the logo could be replaced by __"alternates"__. In the same way that the Walking Stick lamps come in different possible configurations, the logo could also be "configured".
Beyond a logo in perpetual reconfiguration, we imagine being able to use this font with the famous alternative letters contained in the words Walking Stick. Thus, we could consider seeing these differences within other textual content such as titles, main texts, etc.

![](img/specimen_light.png)
![](img/specimen_bold.png)


We consider the elements of fixation of the furniture as __new potential glyphs__, which could intervene as __marker__ and punctuate the textual contents.


![](img/fix2.png)
![](img/fix3.png)
![](img/fix4.png)
![](img/fix5.png)

![](img/fix1.svg)
![](img/fix2.svg)
![](img/fix3.svg)
![](img/fix4.svg)
![](img/fix5.svg)

The __colors__ used in Walking Stick's furniture fixtures and accessories are, in our opinion, a good way to build a color palette for the visual identity. Colors that can be increased if new ones appear in WS's production.
![](img/color.png)


With all these elements, multiples compositions are possibles.

Monogram,

![](img/mono1b.gif)

logos,

![](img/name.gif)

sentences.

![](img/text.png)

In order to create a contrast between the typography used for the logo and the rest, we choose a more classical serif font, __Parangon__, designed by Cédric Rossignol-Brunet in 2019. This open source font is a revival from a font designed by Chauncey H. Griffith in 1935. The letters were designed to stay legible when printed in very small size, even with a low print quality.

![](img/parangon.png)

The typeface designed for WS's logo is monospaced, meaning all letters have the same width, like a typewriter font. We can use this particularity to create an emphasis system based on the Walking Stick glyphs in a ASCII art spirit. This text based visual art, which use a minimal character set and play with multiples combinations to design visuals seemed to us close to Walking Stick's design.

![](img/compo.png)




